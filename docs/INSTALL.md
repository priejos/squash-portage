Installation Instructions
=========================

Author: Jose M. Prieto <<priejos@gmail.com>>  
Date: Jan 26th 2014

Steps
-----

Patch the kernel to support overlayfs, for instance:

	$ cd /usr/src/linux
	$ patch -p1 < overlayfs-kernel-3_6.patch

NOTE: as of Linux kernel 3.18 overlayfs is mainlined in kernel source tree
meaning you do not need to patch Linux source kernel to include support for
it.

Compile kernel to support loop devices, TMPFS (RAMFS), squash filesystems
and overlay filesystems:

	$ cd /usr/src/linux
	$ make menuconfig

Then select following options:

	Device Drivers  --->
	[*] Block devices  --->
	<M>   Loopback device support
	(8)     Number of loop devices to pre-create at init time
	File systems  --->
	<M> Overlay filesystem support
	Pseudo filesystems  --->
	-*- Tmpfs virtual memory file system support (former shm fs)
	[*]   Tmpfs POSIX Access Control Lists
	-*-   Tmpfs extended attributes
	[*] Miscellaneous filesystems  --->
	<M>   SquashFS 4.0 - Squashed file system support
	[*]     Squashfs XATTR support
	[*]     Include support for ZLIB compressed file systems
	[*]     Include support for LZO compressed file systems
	[*]     Include support for XZ compressed file systems

Now compile the Linux kernel again using `genkernel`:

	$ genkernel --oldconfig --install all

Reboot so that new kernel is used.

Install following packages with emerge if not installed yet:

	* sys-fs/squashfs-tools
	* app-misc/realpath

A RAMFS is going to be used as a read-write overlay on top of read-only 
squashed images. You can either set up properly `/dev/shm` or create a new one 
as follows.

If you use `/dev/shm` make sure size is big enough in `/etc/fstab`:

	shm  /dev/shm  tmpfs  nodev,nosuid,noexec,size=<size>   0  0

You can also have a separated RAMFS created for this purpose:

	$ mkdir -p /tmp/squash_portage_shm
	$ mount -t tmpfs -o size=<size> tmpfs /tmp/squash_portage_shm
	$ nano /etc/fstab
	...
	tmpfs  /tmp/squash_portage_shm   tmpfs   defaults,size=<size>  0   0
	...

Move Portage distribution file directory (DISTDIR) out of Portage directory 
(PORTDIR):

	$ emerge --info | grep DISTDIR
	DISTDIR="/usr/portage/distfiles"
	$ mkdir -p /var/squash-portage
	$ mv /usr/portage/distfiles /var/squash-portage
	$ nano /etc/portage/make.conf
	...
	DISTDIR=/var/squash-portage/distfiles
	...

Create the SquashFS images as follows. I am assuming `/var/squash-portage` as new 
root directory for Portage overlays. For example, to compress current standard 
Portage overlay into a SquashFS image:

	$ mkdir -p /var/squash-portage/images
	$ emerge --info | grep PORTDIR
	PORTDIR="/usr/portage"
	$ mksquashfs /usr/portage /var/squash-portage/images/portage.sqfs -comp xz -b 262144

You can repeat above commands for any other overlay you may be using.

Copy attached init scripts squash-portage into `/etc/init.d` and `/etc/conf.d`.

Adjust `/etc/conf.d/squash-portage` to your scenario if default values are not 
applicable to you.

Adjust `/etc/portage/make.conf` and any other overlay manager configuration 
file (e.g. `/etc/layman/layman.cfg`) so that overlay directories point out to 
new location (if default values are used, `/var/squash-portage/...`)

	$ nano /etc/portage/make.conf
	...
	PORTDIR=/var/squash-portage/portage
	PKGDIR=$PORTDIR/packages
	...

	$ nano /etc/layman/layman.cfg
	...
	[MAIN]
	storage   : /var/squash-portage/layman
	cache     : /var/lib/layman/cache
	local_list: /var/lib/layman/overlays.xml
	make_conf : /var/lib/laymay/make.conf
	overlays  : http://www.gentoo.org/proj/en/overlays/repositories.xml
	nocheck  : yes
	...

Add squash_portage init script to default runlevel:

	$ rc-update add squash-portage boot

Start SquashFS Portage Tree:

	$ /etc/init.d/squash-portage start

Adjust your current profile with `eselect profile`:

	$ ls -l /etc/portage/make.profile
	lrwxrwxrwx 1 root root  54 Apr 23 15:35 /etc/portage/make.profile -> /usr/portage/profiles/default/linux/amd64/10.0
	$ rm /etc/portage/make.profile
	$ ln -s /var/portage/portage/profiles/default/linux/amd64/10.0 /etc/portage/make.profile
	$ eselect profile set 1
	$ eselect profile list
	Available profile symlink targets:
	  [1]   default/linux/amd64/10.0 *
	  [2]   default/linux/amd64/10.0/selinux
	  [3]   default/linux/amd64/10.0/desktop
	  [4]   default/linux/amd64/10.0/desktop/gnome
	  [5]   default/linux/amd64/10.0/desktop/kde
	  [6]   default/linux/amd64/10.0/developer
	  [7]   default/linux/amd64/10.0/no-multilib
	  [8]   default/linux/amd64/10.0/server
	  [9]   hardened/linux/amd64
	  [10]  hardened/linux/amd64/selinux
	  [11]  hardened/linux/amd64/no-multilib
	  [12]  hardened/linux/amd64/no-multilib/selinux

