# How to build an overlayfs Linux kernel patch

[OverlayFS][ovl-explain] support is not included yet by default in Linux kernel, so you either 
have two options to find a proper patch for a given kernel version:

1. If available at [dev.openwrt.org](http://dev.openwrt.org) you can download
the patch from there.
2. Or either you can build it up yourself following instructions given next.

*UPDATE!!! Mar 18th 2015
Support for [OverlayFS][ovl-explain] is now included in mainline of Linux source tree as of
version 3.18*

## Steps to build your own overlayfs Linux kernel patch

Let us assume you want to build a Linux kernel patch to include overlayfs support for Linux kernel version x.yy.zz.

First step will be to clone [OverlayFS GIT repository][ovl-git-repo] into your local development box:

    $ git clone git://git.kernel.org/pub/scm/linux/kernel/git/mszeredi/vfs.git linux-overlayfs

Change to the cloned repository and add the [Linux GIT repository][linux-git-repo] as an additional remote:

    $ cd linux-overlayfs
    $ git remote add linux git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
    $ git fetch linux

Now it comes one of the most difficult steps.The developer who maintains the [OverlayFS GIT repository][ovl-git-repo] keeps different branches called as `overlayfs.vXX` for each version of the [OverlayFS][ovl-explain] he releases. You need to pick one which was created based on a Linux source version equal or lower to the version you want to go. Otherwise you will not be able to merge commits to your branch later on. The way you can figure this out is by checking out of those [OverlayFS branches][ovl-git-refs] and display content of Linux `Makefile`.

Let's assume we want to make a patch for branch `overlayfs.v23`. So we will do as follows:

    $ git checkout -b linux-patch-v23 origin/overlayfs.v23
    ...
    $ head -4 Makefile
    VERSION = 3
    PATCHLEVEL = 16
    SUBLEVEL = 0
    EXTRAVERSION = -rc4

In this case for instance [OverlayFS][ovl-explain] author used Linux version 3.16.0-rc4 source code to develop `overlayfs.v23`. The easiest will be then to use this [OverlayFS][ovl-explain] version to make a patch for Linux kernel version 3.16.zz. However you can use that to merge to a higher Linux kernel version as well. Which is not for sure recommended is to use `overlayfs.v23` as a starting point to build a linux kernel patch for Linux version 3.12.zz for instance. In that case you should go for a earlier version of [OverlayFS][ovl-explain].

So now we can merge one of the highest official Linux branches from the [Linux GIT repository][linux-git-repo] into our local branch `linux-patch-v23` which recall it was created out of `overlayfs.v23`. For instance, next command will merge all commit from Linux kernel branch `linux-3.16.y` to our local one:

    $ git branch
      master
    * linux-patch-v23
    $ git merge linux/linux-3.16.y

Note that you can do the same using one of the defined tags in [Linux GIT repository][linux-git-repo]. You can get a list of branches and tags at [here][linux-git-refs].

Finally you are now in a position to build your [OverlayFS][ovl-explain] Linux kernel patch as follows:

    git diff linux/linux-3.16.y > overlayfs-3_16_y.patch

In this case, following our example, we created a patch ready to be applied into a Linux kernel 3.16.zz source code.

# References
[1] [Creating an OverlayFS Patch][adis-ca-overlay-patch]  
[2] [Linux GIT repository][linux-git-repo]  
[3] [Overlay FS GIT repository][ovl-git-repo]

[ovl-explain]: http://en.wikipedia.org/wiki/Overlayfs
[adis-ca-overlay-patch]: http://adis.ca/post/overlayfs-patch/
[linux-git-repo]: https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
[linux-git-refs]: http://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git/refs/
[ovl-git-repo]: https://git.kernel.org/pub/scm/linux/kernel/git/mszeredi/vfs.git
[ovl-git-refs]: https://git.kernel.org/cgit/linux/kernel/git/mszeredi/vfs.git/refs/heads
