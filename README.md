SquashFS Gentoo Portage Tree
============================

Author: Jose M. Prieto <<priejos@gmail.com>>  
Date: Jan 26th 2014


Introduction
------------

Gentoo Portage Tree is the core of Gentoo software distribution tool (emerge). 
It is a very powerful tool developed in [Python][l_python] and [Bash][l_bash]. 
However it has a clear problem in my humble opinion which is the amount of 
diskspace that it requires. This is an issue on systems where there are some 
restrictions in terms of diskspace. Imagine for instance, a Gentoo box with a 
small SSD disk dedicated to hold the OS. 

Therefore the idea behind this *squash-portage* package is to compress the whole
Gentoo Portage Tree as a read-only [SquashFS][l_squashfs] filesystem with an
[OverlayFS][l_overlayfs] on top to make it writable. It is indeed pretty much
similar to the way [OpenWRT][l_openwrt] writes OS images into router available
ROM. This way your Gentoo Portage Tree will be reduced from a some GBs to a few
MBs.

However keep in mind that package *squash-portage* will not free you up from 
the need to from time to time clean up `distfile` directory via `eclean` 
command.

[l_python]: http://www.python.org/
[l_bash]: http://www.gnu.org/software/bash
[l_squashfs]: http://en.wikipedia.org/wiki/SquashFS
[l_overlayfs]: http://en.wikipedia.org/wiki/Overlayfs
[l_openwrt]: https://openwrt.org/


Installation
------------
* See INSTALL file.

Wiki
-----
* See [Wiki page](/priejos/squash-portage/wiki) for more information

Changelog
---------
version 1.0 [Jan 26th 2014]
> Initial version

version 1.1 [Nov 24th 2014]
> There is no new functionality  
> Include documentation directory

version 2.0 [Nov 26th 2014]
> Support for workdir option introduced in overlayfs.v22  
> Better usage of gentoo helper functions ebegin, einfo, etc.  
> New function 'fullsync' running `emerge --sync` [NOT WORKING YET]  

version 2.1 [Mar 15th 2015]
> Set in configuration file whether the underlying overlayfs supports workdir

version 3.0 [Mar 16th 2015]
> OverlayFS has been included in mainline of Linux kernel source tree
> Kernel module has been renamed from overlayfs to overlay

